***Simple node RESTfUL API in express using mongodb***

![nodejs-express-mongo](/uploads/4daaa625c6391d591815c15a4a7fb2ac/nodejs-express-mongo.jpg)


  ***Content***
   
*  [x] CRUD
*  [x] auth
*  [x] jwt
*  [x] morgan
*  [x] moongose
*  [x] auth middelware 
*  [x] upload files

**Resources**

**guide** :
https://www.youtube.com/watch?v=0oXYLzuucwE&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q

**mongodb cloud atlas**:
https://www.mongodb.com/cloud/atlas

**jwt** :
https://jwt.io


**Dependencies** :

*express*:
http://expressjs.com/ 

*mongoose*:
http://mongoosejs.com/

*body-parser*:
https://github.com/expressjs/body-parser 

*morgan*:
https://github.com/expressjs/morgan

*jwt*:
https://github.com/auth0/node-jsonwebtoken

*bcrypt*:
https://github.com/kelektiv/node.bcrypt.js/

*multer*:
https://github.com/expressjs/multer

