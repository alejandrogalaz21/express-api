//Holds the export module
const controller =  {}
//Model
const User = require('../models/user')
//Dependencies
const moongose = require('mongoose')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

//Login the user
//return jwt token
controller.login = (req, res, next) => {
  
  const email = req.body.email
  const password = req.body.password
  
  User.find({email: email})
  .exec()
  .then(users => {
    //user not found
    if (users.length == 0) return res.status(404).json({message: 'User not found', loggin: false})

    const user = users[0]
    //check password
    bcrypt.compare(password, user.password, (e, r) => {
      // ups something was rong
      if (e) res.status(401).json({message: 'Auth Faild', loggin: false})
      // compare success
      if (r) {
        //create the jwt and set options
        const token = jwt.sign(
          { email: user.email, userId: user._id}, 
            process.env.JWT_KEY || 'a2112221684',
            {
              expiresIn: "1h"
            }
          )
         res.status(200).json({message: 'Auth Successful', loggin: true, token: token})
      }  
    })  
  })
  .catch(e => res.status(500).json(r))
}



module.exports = controller