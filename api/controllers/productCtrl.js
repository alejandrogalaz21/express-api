//Holds the export module
const controller =  {}
//Model
const Product = require('../models/product')
//Dependencies
const moongose = require('mongoose')

//GET ALL THE RECORDS
controller.getAll = (req, res, next) => {
  Product.find()
  .select('name price _id productImg')
  .exec()
  .then(result => {
    res.status(201).json(result)
  })
  .catch(error => {
    res.status(500).json({error: error})
  })
}

//ADD A RECORD
controller.add =  (req, res, next) => {
 
  const product = new Product({
    _id: new moongose.Types.ObjectId(),
    name: req.body.name,
    price: req.body.price,
    productImg: req.file.path
  })

  product
  .save()
    .then((result) => {
      res.status(201).json(result)
    })
  .catch(error => {
    res.status(500).json({error: error})
  })
}

//GET ONE RECORD
controller.show = (req, res, next) => {
 const id = req.params.id
  Product.findById(id)
  .select('name price _id productImg')
  .exec()
  .then(result => {
    if (result) res.status(200).json(result)
    else res.status(404).json({message: 'No valid entry found'})
  })
  .catch(error => {
    res.status(500).json({error: error})
  })
}

//UPDATE A RECORD
controller.update = (req, res, next) => {
  const id = req.params.id
  const updateOps = {}
  
  for (const ops of req.body) {
    updateOps[ops.propName] =  ops.value
  }
  
  Product.update({_id: id}, {$set: updateOps})
  .exec()
  .then(result => res.status(200).json(result))
  .catch(error => res.status(500).json({error: error}))
}

//DELETE A RECORD
controller.delete = (req, res, next) => {
  const id = req.params.id
  Product.findByIdAndRemove({_id: id})
  .exec()
  .then(result => res.status(200).json(result))
  .catch(error => res.status(500).json({error: error}))
}




module.exports = controller