//Holds the export module
const controller =  {}
//Model
const User = require('../models/user')
//Dependencies
const moongose = require('mongoose')
const bcrypt = require('bcrypt')


//Retur all records
controller.getAll  = (req, res, next) => {
  User
  .find()
  .select('_id email password')
  .exec()
  .then(r => res.status(200).json(r))
  .catch(e => res.status(500).json(r))
}

//Add a new record
controller.add = (req, res, next) => {
  //Check if the user exist return the users
  User.find({email: req.body.email})
  .exec()
  .then(users => {
    if (users.length != 0) return res.status(409).json({message: 'Mail exist'})
    else {
      bcrypt.hash(req.body.password, 10, (error, hash) => {
        if (error) return res.status(500).json(error)
        else {
          const user = new User({
            _id: new moongose.Types.ObjectId(),
            email: req.body.email,
            password: hash
          })
    
          user.save()
          .then((result) => {
            res.status(201).json({
              message: "User Created",
              userCreated: true,
              data: result
            })
          })
          .catch(error => res.status(500).json({error: error}))
        }
      })   
    } 
  })
  .catch(error => res.status(500).json({error: error}))
}

//Delete a record
controller.delete = (req, res, next) => {
  const id = req.params.id
  User.findByIdAndRemove({_id: id})
  .then(result => res.status(200).json({message: 'User deleted'}))
  .catch(error => res.status(500).json({error: error}))
}

module.exports = controller