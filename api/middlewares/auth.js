const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  //get the token from the headers
  const token = req.headers.authorization.split(" ")[1]
  try {
    //berify the token and decode
    const decoded = jwt.verify(token, process.env.JWT_KEY || 'a2112221684')
    //pass the token data decoded
    req.userData = decoded
    next()
  }
  catch (e) {
    return res.status(401).json({message: 'Unauthorized'})
  }
}