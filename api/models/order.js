const mongoose = require('mongoose')
const Schema = mongoose.Schema

const orderSchema = new Schema({
  _id: Schema.Types.ObjectId,
  product: {
    type: Schema.Types.ObjectId, 
    ref: 'Product',
    required: true
  },
  quantity: {
    type: Number,
    default: 1
  }
})

const model = mongoose.model('Order', orderSchema)

module.exports = model