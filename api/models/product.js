const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  productImg: {
    type: String
  }
})

const model = mongoose.model('Product', productSchema)

module.exports = model