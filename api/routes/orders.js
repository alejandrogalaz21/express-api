const express = require('express')
//enable express router
const router = express.Router()

//GET route/
router.get('/', (req, res, next) => {
  res.status(200).json({
    message: 'Handling GET orders Route.'
  })
})

//POST route/
router.post('/', (req, res, next) => {
  const order  = {
    productId: req.body.productId,
    quantity: req.body.quantity
  }
  res.status(201).json({
    message: 'Handling POST orders Route.',
    order: order
  })
})

//GET route/id
router.get('/:id', (req, res, next) => {
  const id = req.params.id
  res.status(200).json({
    message: 'Handling GET Id orders Route.',
    id: id
  })
})

//DELETE route/id
router.delete('/:id', (req, res, next) => {
  const id = req.params.id
  res.status(200).json({
    message: 'Handling DELETE Id order Route.',
    id: id
  })
})

module.exports = router