const express = require('express')
const router = express.Router()
//Middlewares
const authMiddleware = require('../middlewares/auth')
//Controller's
const productCtrl = require('../controllers/productCtrl')
//Dependencies to save files
const multer = require('multer')
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
   },
  filename: function (req, file, cb) {
    cb(null, file.originalname )
  }
})
//Upload multer config
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 *5
  }, 
  fileFilter: (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') cb(null, true)
    else cb(null, false)
  }
})
//Require express router

//GET route/
router.get('/', productCtrl.getAll)

//POST route/
router.post('/', upload.single('imgFile'), productCtrl.add)

//GET route/id
router.get('/:id', productCtrl.show)

//PATCH route/id
router.patch('/:id', productCtrl.update)

//DELETE route/id
router.delete('/:id', productCtrl.delete)

module.exports = router