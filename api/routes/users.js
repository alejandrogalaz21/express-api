const express = require('express')
const router = express.Router()
//Middlewares
const authMiddleware = require('../middlewares/auth')
//Controller's
const userCtrl = require('../controllers/userCtrl')
const authCtrl = require('../controllers/authCtrl')

//GET route/
//GET all
router.get('/', authMiddleware, userCtrl.getAll)

//POST route/
//Register users
router.post('/singup', userCtrl.add)

//DELETE route/id
router.delete('/:id', userCtrl.delete)

//POST route/
// Login
router.post('/login', authCtrl.login)

module.exports = router