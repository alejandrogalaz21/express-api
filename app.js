const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParsers = require('body-parser')
const mongoose = require('mongoose')

// Import Routes.
const productsRoutes = require('./api/routes/products')
const ordersRoutes = require('./api/routes/orders')
const usersRoutes = require('./api/routes/users')

//Connect to mongoDB
mongoose.connect(`mongodb+srv://alejandrogalaz21:hunter21@cluster0-czfnf.mongodb.net/test?retryWrites=true`)

// Console the request
app.use(morgan('dev'))

app.use('/uploads',express.static('uploads'))

// Parse tje requests
app.use(bodyParsers.urlencoded({ extended: false }))
app.use(bodyParsers.json())

//Handle Cors
app.use((req, res, next) => {
  res.header('Acces-Control-Allow-Origin', '*')
  res.header('Acces-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
  
  if (req.method === 'OPTIONS') {
    res.header('Acces-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE')
    return res.status(200).json({})
  }

  next()
})

// Set Routes.
app.use('/products', productsRoutes)
app.use('/orders', ordersRoutes)
app.use('/users', usersRoutes)

//Handle not found error
app.use((req, res, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

//Handle all types of errors
app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: {
      message: error.message
    }
  })
})

module.exports = app