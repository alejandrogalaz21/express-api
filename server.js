const http = require('http')
const app = require('./app')
//set the port of the server
const port = process.env.PORT || 3000
//pass the express app
const server = http.createServer(app)
//Listen in port
server.listen(port)